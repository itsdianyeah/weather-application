import React from "react";
// import Autosuggest from 'react-autosuggest';
import Titles from "./components/Titles";
// import Form from "./components/Form";
import WeatherView from "./components/WeatherView";
import Something from "./components/Something";
import Navigation from "./components/Navigation";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";


class App extends React.Component {
    render() {
        return (
                <div className="wrapper">
                    <div className="container">
                        <div className="row">
                            <div className="col-7 form-container borders">
                                <Router>
                                    <div>
                                        <Titles />
                                        <Navigation />
                                        <Switch>
                                            <Route path="/something" component={Something} />
                                            <Route path="/weather" component={WeatherView} />
                                        </Switch>
                                    </div>
                                </Router>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default App;
