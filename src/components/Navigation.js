import React from "react";
import { BrowserRouter as Router, Link } from "react-router-dom";

const Navigation = (props) => {
    return (
      <div>
        <Router>
          <ul>
            <li>
                <Link to="/">Home</Link>
            </li>
            <li>
                <Link to="/something">Something</Link>
            </li>
            <li>
                <Link to="/weather">Weather</Link>
            </li>
          </ul>  
        </Router>
      </div>
    )
  };

export default Navigation;